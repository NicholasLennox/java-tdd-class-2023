package no.noroff.accelerate.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FBConverterTest {

    @Test
    public void convert_nonFbNumber_shouldReturnNumberAsString() {
        // Ar
        int number = 1;
        FBConverter fbConverter = new FBConverter();
        String expected = "1";
        // Ac
        String actual = fbConverter.convert(number);
        // As
        assertEquals(expected, actual);
    }

    @Test
    public void convert_fizzNumber_shouldReturnFizz() {
        // Ar
        int number = 3;
        FBConverter fbConverter = new FBConverter();
        String expected = "Fizz";
        // Ac
        String actual = fbConverter.convert(number);
        // As
        assertEquals(expected, actual);
    }

    @Test
    public void convert_buzzNumber_shouldReturnBuzzAldrin() {
        // Ar
        int number = 5;
        FBConverter fbConverter = new FBConverter();
        String expected = "Buzz";
        // Ac
        String actual = fbConverter.convert(number);
        // As
        assertEquals(expected, actual);
    }

    @Test
    public void convert_FizzBuzzNumber_shouldReturnFizzBuzz() {
        // Ar
        int number = 15;
        FBConverter fbConverter = new FBConverter();
        String expected = "FizzBuzz";
        // Ac
        String actual = fbConverter.convert(number);
        // As
        assertEquals(expected, actual);
    }
}